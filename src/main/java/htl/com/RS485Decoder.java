package htl.com;

import java.lang.ref.WeakReference;

import com.google.common.eventbus.EventBus;

import htl.com.ComDecoder.Decoder;

public class RS485Decoder extends Decoder {

	public static WeakReference<EventBus> eventBus;

	public static void init(EventBus bus) {
		eventBus = new WeakReference<EventBus>(bus);
		TempDecoder.constructorMap.put(PID.RS485_Answer, RS485Decoder::new);
	}

	@Override
	public void decode(String s, long timestamp) {
		String[] parts = s.split(",");
		int addr = Integer.parseInt(parts[0], 16);
		int error = Integer.parseInt(parts[1], 16);
		SED_RS485Event event;
		if (parts.length >= 3) {
			event = new SED_RS485Event(error, addr, parts[2], timestamp);
		}
		else {
			event = new SED_RS485Event(error, addr, "No Data", timestamp);
		}
		EventBus bus = eventBus.get();
		if (bus != null) {
			bus.post(event);
		}
	}

	public static class SED_RS485Event {

		public static final int ERR_INPUT_VOLTAGE = 0x01;
		public static final int ERR_ANGLE_LIMIT = 0x02;
		public static final int ERR_OVERHEATING = 0x04;
		public static final int ERR_RANGE = 0x08;
		public static final int ERR_CHECKSUM = 0x10;
		public static final int ERR_OVERLOAD = 0x20;
		public static final int ERR_INSTRUCTION = 0x40;
		public static final int ERR_WATCHDOG = 0x80;

		public final int error;
		public final int address;
		public final String data;
		public final long timestamp;

		public SED_RS485Event(int error, int address, String data, long timestamp) {
			super();
			this.error = error;
			this.address = address;
			this.data = data;
			this.timestamp = timestamp;
		}

	}

}
