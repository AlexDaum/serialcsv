package htl.com;

import java.util.ArrayList;
import java.util.List;

import javax.print.attribute.IntegerSyntax;

import com.google.common.primitives.UnsignedBytes;

import jssc.SerialPort;
import jssc.SerialPortException;

public class CommandEncoder {
	private SerialPort port;

	public CommandEncoder(SerialPort port) {
		this.port = port;
	}

	public void sendEvent(Command com) throws SerialPortException {

		sendEvent(com, "");
	}

	public void sendEvent(Command com, String payload) throws SerialPortException {
		byte[] bytes = com.apply(payload);
		List<Integer> list = new ArrayList<>();
		list.add(0xAA);
		char[] hex = Integer.toHexString(com.ordinal()).toCharArray();
		if (hex.length == 0)
			list.add((int) '0');
		else {
			for (char c : hex) {
				list.add((int) c);
			}
		}
		list.add((int) ':');
		for (byte c : bytes) {
			list.add(UnsignedBytes.toInt(c));
		}
		list.add((int) '\n');
		int[] array = list.stream().mapToInt(i -> i).toArray();
		port.writeIntArray(array);
	}

}
