package htl.com;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import jssc.SerialPort;

public class ComDecoder {
	private long timeoffs;
	private int lastTime = 0;

	public ComDecoder(long timeoffs) {
		this.timeoffs = timeoffs;
	}

	public void receive(String s) {
		receive(s, System.currentTimeMillis());
	}

	public void receive(String s, long timestamp) {

		int tsbegin = s.indexOf(';');
		if (tsbegin == -1)
			throw new IllegalArgumentException("No ; in packet");
		int payloadbegin = s.indexOf(':');
		if (payloadbegin == -1)
			throw new IllegalArgumentException("No : in packet");
		if (s.lastIndexOf(':') != payloadbegin)
			throw new IllegalArgumentException("More than one : in packet");

		String hexId = s.substring(0, tsbegin);
		int id = Integer.parseInt(hexId, 16);
		if (id >= PID.values().length) {
			throw new IllegalArgumentException("Unknown packet ID " + id);
		}

		PID pid = PID.values()[id];

		String ts = s.substring(tsbegin + 1, payloadbegin);
		int time = Integer.parseInt(ts, 16);
		if (time < lastTime && pid != PID.TimeReset) {
			// overrun
			System.out.println("Overflowed time");
			timeoffs += Integer.MAX_VALUE;
		}
		lastTime = time;

		Supplier<Decoder> constructor = Decoder.constructorMap.get(pid);
		if (constructor == null) {
			if (pid == PID.TimeReset)
				return;// TimeReset does not have to be handled
			throw new IllegalArgumentException("No Decoder for PID " + pid + " is known");
		}
		Decoder dec = constructor.get();
		dec.decode(s.substring(payloadbegin + 1), timeoffs + time);
	}

	protected abstract static class Decoder {
		protected static Map<PID, Supplier<Decoder>> constructorMap = new HashMap<>();

		/**
		 * Decode the custom message, this has no ID in front anymore
		 * 
		 * @param s
		 */
		public abstract void decode(String s, long timestamp);
	}

	public void setTimeoffs(long timeoffs) {
		this.timeoffs = timeoffs;
	}
	
	
}
