package htl.com;

import java.util.function.Consumer;
import java.util.function.Function;

public enum Command implements Function<String, byte[]> {
	NOP, SYNC, START, STOP, SPEED, ENABLE, DISABLE, RS485;

	Function<String, byte[]> callback;

	public void setPayloadMethod(Function<String, byte[]> r) {
		callback = r;
	}

	public void setCallback(Consumer<String> c) {
		callback = (s) -> {
			c.accept(s);
			return s.getBytes();
		};
	}

	@Override
	public byte[] apply(String t) {
		if (callback != null)
			return callback.apply(t);
		return t.getBytes();
	}


}
