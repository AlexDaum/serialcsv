package htl.com;

import java.lang.ref.WeakReference;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import com.google.common.eventbus.EventBus;

import htl.com.ComDecoder.Decoder;

public class TempDecoder extends Decoder {

	public static WeakReference<EventBus> eventBus;

	static {
		TempDecoder.constructorMap.put(PID.Temperature, TempDecoder::new);
	}

	public static void init(EventBus bus) {
		eventBus = new WeakReference<EventBus>(bus);
	}

	public TempDecoder() {

	}

	double calculateTemperature(int adc) {
		// Temperature between -135 to 213°C maps to 0 -> 4096
		// Difference is 348
		return (adc / 4096.0 * 347) - 135;
	}

	@Override
	public void decode(String s, long ts) {
		String[] parts = s.split(",");
		double[] temps = new double[parts.length];
		for (int i = 0; i < parts.length; i++) {
			temps[i] = calculateTemperature(Integer.parseInt(parts[i], 16));
		}
		TemperatureEvent event = new TemperatureEvent(temps, ts); // TODO fix timestamp
		EventBus bus = eventBus.get();
		if (bus == null) {
			throw new IllegalStateException("EventBus is not accessible!");
		}
		bus.post(event);
	}

	public static class TemperatureEvent {
		public final long timestamp;
		public final double[] temperatures;

		private TemperatureEvent(double[] temperatures, long timestamp) {
			this.temperatures = temperatures;
			this.timestamp = timestamp;
		}

		@Override
		public String toString() {
			StringBuilder b = new StringBuilder();
			b.append(timestamp);
			b.append(';');
			for (int i = 0; i < temperatures.length; i++) {
				b.append(String.format("%.1f", temperatures[i]));
				if (i != temperatures.length - 1)
					b.append(';');
			}
			return b.toString();
		}

		public String prettyPrint() {
			DateTimeFormatter dtf = DateTimeFormatter.ISO_LOCAL_TIME;
			LocalDateTime time = LocalDateTime.ofEpochSecond(System.currentTimeMillis() / 1000,
					(int) ((System.currentTimeMillis() % 1000) * 1000000), ZoneOffset.UTC);
			StringBuilder buf = new StringBuilder();
			buf.append(dtf.format(time));
			for(int i = 0; i < temperatures.length; i++) {
				buf.append(String.format("\t%.2f", temperatures[i]));
			}
			return buf.toString();
		}
	}
}
