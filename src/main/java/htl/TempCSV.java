package htl;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import htl.com.ComDecoder;
import htl.com.Command;
import htl.com.CommandEncoder;
import htl.com.RS485Decoder;
import htl.com.RS485Decoder.SED_RS485Event;
import htl.com.TempDecoder;
import htl.com.TempDecoder.TemperatureEvent;
import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import jssc.SerialPortList;

public class TempCSV implements SerialPortEventListener {

	private SerialPort port;
	private boolean alive = true;
	private Writer csvWriter;
	private Writer sedLog;
	private long lastTimestamp;
	private ComDecoder decoder = new ComDecoder(System.currentTimeMillis());
	private CommandEncoder encoder;
	private boolean running = false;
	private int delay = 1000;

	public TempCSV(Path output, EventBus eventBus) throws IOException, SerialPortException {

		lastTimestamp = System.currentTimeMillis();

		csvWriter = Files.newBufferedWriter(output);
		sedLog = Files
				.newBufferedWriter(output.resolveSibling(output.getFileName().toString().split("\\.")[0] + "_sed.txt"));
		this.selectPort();
		encoder = new CommandEncoder(port);
		syncTime();
		encoder.sendEvent(Command.START);

		// Create the Timer, that will resync, when not getting data for >10*delay
		Timer t = new Timer(true);
		TimerTask task = new KeepaliveTask();
		t.schedule(task, 1000, 1000);
		TimerTask ping = new PingTask();
		t.schedule(ping, 10000, 10000);

		Command.START.setCallback(s -> {
			lastTimestamp = System.currentTimeMillis();
			running = true;
		});
		Command.STOP.setCallback(s -> running = false);
		Command.SPEED.setCallback(s -> {
			delay = Math.max(2000, Integer.parseInt(s));
			lastTimestamp = System.currentTimeMillis();
		});
//		Command.RS485.setPayloadMethod(this::generateRS485Command);

		eventBus.register(this);
	}

	public void selectPort() throws SerialPortException {
		String[] portNames = SerialPortList.getPortNames();
		if (portNames.length > 1) {
			System.out.println("Found multiple ports, please select the correct one:");
			System.out.println("If you do not know which one you need, unplug the device and watch"
					+ " which port appears when you plug it back in");
			System.out.println("If no port appears when plugging in the device, check if the correct"
					+ " drivers are installed and check if there is a udev rule on linux systems");
			for (int i = 0; i < portNames.length; i++) {
				System.out.println(i + ") " + portNames[i]);
			}
			// do not close System.in
			@SuppressWarnings("resource")
			Scanner scanner = new Scanner(System.in);
			int idx;
			do {
				idx = scanner.nextInt();
			} while (idx < 0 || idx >= portNames.length);
			port = new SerialPort(portNames[idx]);
		} else if (portNames.length == 1) {
			System.out.println("Only " + portNames[0] + " found, connecting");
			port = new SerialPort(portNames[0]);
		} else {
			System.out.println("No Serial Devices connected, aborting");
			System.out.println("If no port appears when plugging in the device, check if the correct"
					+ " drivers are installed and check if there is a udev rule on linux systems");
			System.exit(1);
		}

		try {
			csvWriter.write(
					"Timestamp (ms); Temperature 1 (°C); Temperature 2 (°C); Temperature 3 (°C); Temperature 4 (°C)\n");
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

		port.openPort();
		port.setParams(115200, 8, 1, SerialPort.PARITY_NONE);

		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			try {
				if (port.isOpened())
					port.closePort();
				csvWriter.close();
				sedLog.close();
			} catch (SerialPortException | IOException e) {
				e.printStackTrace();
			}
		}));
		port.addEventListener(this);
	}

	private void syncTime() throws SerialPortException {
		encoder.sendEvent(Command.SYNC);
		encoder.sendEvent(Command.START);
		encoder.sendEvent(Command.SPEED, "1000");
	}

	private static final EventBus globalEventBus = new EventBus();

	private class KeepaliveTask extends TimerTask {
		@Override
		public void run() {
			long diff = System.currentTimeMillis() - lastTimestamp;

			if ((diff > delay * 10) && running) {
				DateTimeFormatter dtf = DateTimeFormatter.ISO_LOCAL_TIME;
				LocalTime time = LocalTime.now();

				System.out.printf("Timeout@ %d (%s), trying to re-initialize board now\n", System.currentTimeMillis(),
						dtf.format(time));
				try {
					syncTime();
				} catch (SerialPortException e) {
					e.printStackTrace();
				}
				lastTimestamp = System.currentTimeMillis();
			}
		}
	}

	private class PingTask extends TimerTask {
		@Override
		public void run() {
			try {
				encoder.sendEvent(Command.RS485, "0:1:");
			} catch (SerialPortException e) {
				e.printStackTrace();
			}
		}

	}

	public static void main(String[] args) throws IOException {
		TempDecoder.init(globalEventBus);
		RS485Decoder.init(globalEventBus);
		if (args.length < 1) {
			System.out.println("Usage: tempcsv.jar output.csv");
			System.exit(1);
		}

		try {
			TempCSV tcsv = new TempCSV(Paths.get(args[0]), globalEventBus);

			while (tcsv.alive) {

				Scanner s = new Scanner(System.in);
				String line = s.nextLine();
				try {
					String[] command = line.split(" ");
					Command com = Command.valueOf(command[0].toUpperCase());
					if (command.length > 1)
						tcsv.encoder.sendEvent(com, command[1]);
					else
						tcsv.encoder.sendEvent(com);
				} catch (IllegalArgumentException e) {
				}
				if (line.contains("exit")) {
					System.out.println("Received exit command, Shutting Down");
					tcsv.close();
					s.close();
					System.exit(0);
				}
			}
		} catch (SerialPortException e) {
			e.printStackTrace();
		}
	}

	public void close() throws IOException, SerialPortException {
		csvWriter.flush();
		port.closePort();
	}

	// private double lastTemps[] = new double[] { 0, 0, 0, 0 };
	private int lineCtr = 0;

	@Subscribe
	public void handleTempEvent(TemperatureEvent event) throws IOException {
		csvWriter.write(event.toString());
		csvWriter.write('\n');
		lineCtr++;

		System.out.printf("%d:\t%s\r", lineCtr, event.prettyPrint());
	}

	@Subscribe
	public void handleRS485Answer(SED_RS485Event event) {

		try {
			sedLog.write("Response from " + event.address + "\n");

			if ((event.error & SED_RS485Event.ERR_ANGLE_LIMIT) != 0)
				sedLog.write("Motor is on angle Limit\n");
			if ((event.error & SED_RS485Event.ERR_CHECKSUM) != 0)
				sedLog.write("Checksum error\n");
			if ((event.error & SED_RS485Event.ERR_INPUT_VOLTAGE) != 0)
				sedLog.write("Input Voltage is wrong\n");
			if ((event.error & SED_RS485Event.ERR_INSTRUCTION) != 0)
				sedLog.write("Invalid Instruction!\n");
			if ((event.error & SED_RS485Event.ERR_OVERHEATING) != 0)
				sedLog.write("Overheating\n");
			if ((event.error & SED_RS485Event.ERR_OVERLOAD) != 0)
				sedLog.write("Overload\n");
			if ((event.error & SED_RS485Event.ERR_RANGE) != 0)
				sedLog.write("Range\n");
			if ((event.error & SED_RS485Event.ERR_WATCHDOG) != 0)
				sedLog.write("Encountered watchdog reset\n");
			if (event.error != 0)
				System.err.println("Error from SED: " + event.error);
			sedLog.write("Response: " + event.data + "\n\n");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Indicates if a start of frame was received. If this is false and an end of
	 * frame is received, then data might be missing at the front and the frame is
	 * invalid
	 */
	private boolean frameSync = false;
	private StringBuffer dataBuffer = new StringBuffer();

	@Override
	public void serialEvent(SerialPortEvent serialPortEvent) {
		if (serialPortEvent.isRXCHAR()) {
			lastTimestamp = System.currentTimeMillis();
			try {
				int[] s = port.readIntArray();
				if (s == null) {
					System.err.printf("String is null at %d, this should not be possible\n",
							System.currentTimeMillis());
					return;
				}
				for (int c : s) {
					if (c == 0xAA) { // Start of Frame
						frameSync = true;
						dataBuffer.setLength(0); // clear
					} else if (!frameSync)
						continue;
					else if (c == '\n') { // End of Frame
						if (frameSync) {
							if (decoder == null) {
								System.err.println("Whoops!");
								return;
							}
							decoder.receive(dataBuffer.toString());
							// System.out.println(dataBuffer.toString());
						}
						frameSync = false;
					} else if (c == '\r') {
						// ignore
					} else {
						dataBuffer.append((char) c);
					}
				}
			} catch (SerialPortException e) {
				e.printStackTrace();
			}
		}
	}
}
